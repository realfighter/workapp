<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
    <%@include file="/common/common_meta.jsp" %>
    <style type="text/css">
    	#login .n-invalid {border: 1px solid #f00;}
    </style>
</head>
<body>
	<div data-role="page">
	    <div data-role="header">
	        <h1>用户登录</h1>
	    </div>
	    <form action="login.honzh" method="post" id="login">
		    <div data-role="content">
		        <div data-role="fieldcontain">
		        	<label for="name">用户名:</label>
		        	<input type="text" name="user.name" id="name" placeholder="请输入用户名" data-rule="用户名:required;length[4~]"/>
		       	</div>	
		       	<div data-role="fieldcontain">    
		       		<label for="password">密码:</label>    
		       		<input type="password" name="user.pwd" id="pwd" placeholder="请输入密码" data-rule="密码:required;length[4~]"/>
		       	</div>	
		       	<div data-role="fieldcontain">
					<fieldset data-role="controlgroup">
						<legend></legend>
						<input type="checkbox" name="isremeber" id="isremember" class="custom" />
						<label for="isremember">记住用户名</label>
					</fieldset>
				</div>
				<div data-role="controlgroup">
					<button type="button" id="sumitbtn">登录</button>
					<a href="register.honzh" data-role="button">注册</a>
				</div>
		    </div>
	    </form>
	    <%@include file="/common/common_footer.jsp" %>
	</div>
</body>
<script type="text/javascript">
	var COOKIE_NAME = "honzh_login_username";
	if ($.cookie(COOKIE_NAME)){
	    $("#name").val($.cookie(COOKIE_NAME));
	    $("#pwd").focus();
	    $("#isremember").attr('checked', true);
	} else {
		$("#name").focus();
	}
	$("#sumitbtn").click(function(){
		if($("#isremember").attr("checked")){
			$.cookie(COOKIE_NAME, $("#name").val(), { path: '/', expires: 15 });
		} else {
			$.cookie(COOKIE_NAME, null, { path: '/' });
		}
		$("#login").submit();
	});
</script>
</html>