<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
    <%@include file="/common/common_meta.jsp" %>
    <style type="text/css">
    	#register .n-invalid {border: 1px solid #f00;}
    </style>
</head>
<body>
	<div data-role="page">
	    <div data-role="header">
	        <h1>用户注册</h1>
	    </div>
	    <form action="register2DB.honzh" method="post" id="register">
		    <div data-role="content">
		        <div data-role="fieldcontain">
		        	<label for="name">用户名:</label>
		        	<input type="text" name="user.name" placeholder="请输入用户名{4位以上}" data-rule="用户名:required;length[4~]"/>
		       	</div>	
		       	<div data-role="fieldcontain">    
		       		<label for="password">密码:</label>    
		       		<input type="password" name="user.pwd" id="pwd" placeholder="请输入密码{4位以上}" data-rule="密码:required;length[4~]"/>
		       	</div>	
		       	<div data-role="fieldcontain">
		       		<label for="repeatpassword">确认密码:</label>    
		       		<input type="password" placeholder="请确认密码{4位以上}" data-rule="确认密码:required;match(pwd)"/>
		       	</div>
		       	<div data-role="fieldcontain">
		        	<label for="email">邮箱地址:</label>
		        	<input type="text" name="user.email" placeholder="请输入邮箱地址" data-rule="邮箱地址:required;email"/>
		       	</div>	
		       	<div data-role="fieldcontain">
		       		<label for="realname">真实姓名</label>
		       		<input type="text" name="user.realname" placeholder="请输入真实姓名" data-rule="真实姓名:required;length[2~4]"/>
		       	</div>
				<div data-role="controlgroup">
					<button type="submit">立即注册</button>
					<a href="index.honzh" data-role="button">返回登录</a>
				</div>
		    </div>
	    </form>
	    <%@include file="/common/common_footer.jsp" %>
	</div>
</body>
</html>