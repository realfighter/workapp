package com.honzh.work.config;

import com.alibaba.druid.filter.stat.MergeStatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.honzh.work.controller.AppController;
import com.honzh.work.model.Mail;
import com.honzh.work.model.Note;
import com.honzh.work.model.NoteWeek;
import com.honzh.work.model.User;
import com.honzh.work.model.UserMail;
import com.honzh.work.model.Weekend;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.FakeStaticHandler;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.spring.SpringPlugin;
import com.jfinal.render.ViewType;

/**
 * 
 * @ClassName: AppConfig
 * @Description: App配置
 * @author Realfighter
 * @date 2015-3-12 下午01:20:21
 * 
 */
public class AppConfig extends JFinalConfig {

	@Override
	public void configConstant(Constants me) {
		me.setDevMode(Boolean.parseBoolean(getProperty("devMode")));
		me.setBaseViewPath("/WEB-INF/pages");
		me.setViewType(ViewType.JSP);
	}

	@Override
	public void configHandler(Handlers me) {
		me.add(new FakeStaticHandler(".honzh"));
	}

	@Override
	public void configInterceptor(Interceptors me) {
	}

	@Override
	public void configPlugin(Plugins me) {
		// 增加Druid连接池插件配置
		DruidPlugin druidPlugin = new DruidPlugin(getProperty("jdbcUrl"), getProperty("user"), getProperty("password")
				.trim());
		druidPlugin.addFilter(new MergeStatFilter());
		druidPlugin.setMaxActive(150);
		WallFilter wall = new WallFilter();
		wall.setDbType(getProperty("dbType"));
		druidPlugin.addFilter(wall);
		me.add(druidPlugin);

		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		me.add(arp);

		// 添加映射关系
		arp.addMapping("mst_note", Note.class);// 工作日志
		arp.addMapping("sys_user", User.class);// 用户信息
		arp.addMapping("mst_weekend", Weekend.class);// 月休息日
		arp.addMapping("mst_note_week", NoteWeek.class);// 周计划
		arp.addMapping("sys_usermail", UserMail.class);// 用户绑定邮箱
		arp.addMapping("sys_mail", Mail.class);// 邮件发送记录

		// 增加spring支持
		me.add(new SpringPlugin("classpath*:/spring/applicationContext.xml"));
	}

	@Override
	public void configRoute(Routes me) {
		me.add("/", AppController.class);
	}

	@Override
	public String getProperty(String key) {
		return loadPropertyFile("config.properties").getProperty(key);
	}

}
