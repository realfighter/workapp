package com.honzh.work.model;

import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class User extends Model<User> {

	public static final User dao = new User();

	/**
	 * 根据用户名和密码获取用户
	 * 
	 * @Title: findLoginUser
	 * @author Realfighter
	 * @param name
	 * @param pwd
	 * @return User
	 * @throws
	 */
	public User findLoginUser(String name, String pwd) {
		return findFirst("select * from sys_user where name=? and pwd=?", name, pwd);
	}

}
